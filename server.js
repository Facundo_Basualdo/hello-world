var http = require("http");
var url = require("url");
var fs = require("fs");

function iniciar(route, handle) {
  function onRequest(request, response) {
    var pathname = url.parse(request.url).pathname;
    console.log("Peticion para " + pathname + " recibida.");

    route(handle, pathname);
    
    fs.readFile('HelloWorld.html',function(err,data){
      response.writeHead(200, {"Content-Type": "text/html"});
      response.write(data);
      response.end();
    });
  }

  http.createServer(onRequest).listen(8888);
  console.log("Servidor Iniciado.");
}

exports.iniciar = iniciar;

// http://localhost:8888/
